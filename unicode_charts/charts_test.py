# coding: utf-8

import pytest

from . import charts


def test_bar_chart_empty():
    assert charts.bar_chart([]) == ''


def test_bar_chart():
    assert charts.bar_chart([0, 1, 2, 3, 4, 5, 6, 7, 8]) == ' ▁▂▃▄▅▆▇█'


def test_bar_chart_mixed():
    assert charts.bar_chart([5, 1, 5, 0, 3, 6, 8]) == '▅▁▅ ▃▆█'


def test_bar_chart_scaled():
    assert charts.bar_chart(list(range(0, 33))) \
        == '  ▁▁▁▁▂▂▂▂▃▃▃▃▄▄▄▄▅▅▅▅▆▆▆▆▇▇▇▇███'


def test_bar_chart_min_max():
    assert charts.bar_chart([5, 1, 3], min_value=0, max_value=8) == '▅▁▃'


def test_bar_chart_min_non_zero():
    assert charts.bar_chart([75, 71, 73, 78]) == '▅▁▃█'


@pytest.mark.parametrize('data,plot', [
    ([], ''),
    ([1], ' ╪ '),
    ([2, 2, 2, 2, 3, 3, 3, 4, 4, 5], '  ═╪═─ '),
    ([1, 2, 3, 4, 5, 6, 7, 8, 9, 10], ' ──═══╪══──     '),
    ([115, 81, 50, 69, 141, 35, 223, 135, 211, 192, 222, 159, 160, 255, 18, 27,
      115, 140, 108],
     ' ──════╪═══───       '),
])
def test_box_plot(data, plot):
    assert charts.box_plot(data) == plot


@pytest.mark.parametrize('data,mark,plot', [
    ([], None, ''),
    ([], 0, ''),
    ([], 1, ''),
    ([1], None, ' ╪ '),
    ([1], 0, ' ̊╪ '),
    ([1], 1, ' ╪̊ '),
    ([1], 2, ' ╪ ̊'),
    ([1], 3, ' ╪ '),
    ([1, 2, 3, 4, 5], None, ' ─═╪═─ '),
    ([1, 2, 3, 4, 5], 0, ' ̊─═╪═─ '),
    ([1, 2, 3, 4, 5], 1, ' ̊─═╪═─ '),
    ([1, 2, 3, 4, 5], 2, ' ─̊═╪═─ '),
    ([1, 2, 3, 4, 5], 3, ' ─═╪̊═─ '),
    ([1, 2, 3, 4, 5], 4, ' ─═╪═̊─ '),
    ([1, 2, 3, 4, 5], 5, ' ─═╪═─̊ '),
    ([1, 2, 3, 4, 5], 6, ' ─═╪═─ ̊'),
    ([1, 2, 3, 4, 5], 7, ' ─═╪═─ '),
])
def test_box_plot_mark(data, mark, plot):
    assert charts.box_plot(data, mark=mark) == plot


@pytest.mark.parametrize('distribution,plot', [
    ([0, 0, 0, 0, 0, 0, 0], '╪'),
    ([0, 5, 5, 5, 5, 5, 10], '     ╪     '),
    ([0, 1, 2, 3, 4, 5, 6], ' ─═╪═─ '),
    ([0, 5, 10, 15, 20, 25, 30], '     ─────═════╪═════─────     '),
])
def test_box_plot_row(distribution, plot):
    canvas = charts.Range(distribution[0], distribution[-1])
    dist = charts.Distribution(*distribution[1:-1])
    assert charts._box_plot_row(canvas, dist) == plot


@pytest.mark.parametrize('mark,row', [
    (None, ' ─═╪═─ '),
    (0, ' ̊─═╪═─ '),
    (0.14, ' ̊─═╪═─ '),
    (0.28, ' ─̊═╪═─ '),
    (0.42, ' ─═̊╪═─ '),
    (0.56, ' ─═╪̊═─ '),
    (0.7, ' ─═╪═̊─ '),
    (0.84, ' ─═╪═─̊ '),
    (1.0, ' ─═╪═─ ̊'),
])
def test_mark_box_plot_row(mark, row):
    assert charts._mark_box_plot_row(' ─═╪═─ ', mark) == row


@pytest.mark.parametrize('minimum,maximum,origin,edge', [
    (0, 0, 0, 1),
    (1, 1, 0, 2),
    (2, 2, 1, 3),
    (200, 200, 199, 201),
    (0, 1, 0, 2),
    (2, 5, 0, 6),
    (3.4, 10, 0, 12),
    (7.4, 10, 6, 12),
    (12, 15, 10, 16),
    (12, 55, 0, 60),
    (148.5, 154.79, 145, 155),
    (68.45, 807, 0, 1000),
    (2587, 11695, 0, 15000),
])
def test_box_plot_range(minimum, maximum, origin, edge):
    dist = charts.Distribution(minimum, None, None, None, maximum)
    assert charts.Range.from_dist(dist) == charts.Range(origin, edge)


@pytest.mark.parametrize('data,minimum,q1,median,q3,maximum', [
    ([1], 1, 1, 1, 1, 1),
    ([1, 1], 1, 1, 1, 1, 1),
    ([1, 1, 1], 1, 1, 1, 1, 1),
    ([1, 2], 1, 1, 1.5, 2, 2),
    ([1, 1, 2, 2], 1, 1, 1.5, 2, 2),
    ([1, 2, 3], 1, 1, 2, 3, 3),
    ([1, 2, 3, 4, 5], 1, 1.5, 3, 4.5, 5),
    ([1, 2, 3, 4, 5, 6, 7, 8], 1, 2.5, 4.5, 6.5, 8),
    ([1, 2, 3, 4, 5, 6, 7, 8, 9], 1, 2.5, 5, 7.5, 9),
    ([1, 2, 3, 4, 5, 6, 7, 8, 9, 10], 1, 3, 5.5, 8, 10),
])
def test_box_plot_distribution(data, minimum, q1, median, q3, maximum):
    assert charts.Distribution.from_data(data) \
        == charts.Distribution(minimum, q1, median, q3, maximum)


@pytest.mark.parametrize('data,scale,result', [
    (charts.Range(1, 2), 1.0, charts.Range(1, 2)),
    (charts.Distribution(1, 2, 3, 4, 5), 1.0,
     charts.Distribution(1, 2, 3, 4, 5)),
    (charts.Range(1, 2), 0.5, charts.Range(0, 1)),
    (charts.Range(1, 2), 2.0, charts.Range(2, 4)),
])
def test_box_plot_scale(data, scale, result):
    assert data * scale == result


@pytest.mark.parametrize('mark,origin,edge,pos', [
    (None, 0, 1, None),
    (-1, 0, 1, None),
    (0, 0, 1, 0),
    (0.5, 0, 1, 0.5),
    (1, 0, 1, 1),
    (2, 0, 1, None),
    (None, 0, 10, None),
    (-1, 0, 10, None),
    (0, 0, 10, 0.0),
    (5, 0, 10, 0.5),
    (10, 0, 10, 1.0),
    (11, 0, 10, None),
])
def test_mark_pos(mark, origin, edge, pos):
    canvas = charts.Range(origin, edge)
    assert canvas.mark_pos(mark) == pos


@pytest.mark.parametrize('dist,width,row', [
    ((0, 1, 2, 3, 4, 5, 6), 5, ' ─╪─ '),
    ((0, 1, 2, 3, 4, 5, 6), 20, '    ───═══╪═══───   '),
    ((0, 10, 20, 25, 30, 40, 50), 20, '     ────══╪═────   '),
])
def test_box_plot_row2(dist, width, row):
    canvas = charts.Range(dist[0], dist[-1])
    dist = charts.Distribution(*dist[1:-1])
    assert charts._box_plot_row2(canvas, dist, width=width) == row


@pytest.mark.parametrize('dist,width,rows', [
    ((0, 1, 2, 3, 4, 5, 6), 5, {
        ' ╪═─ ', ' ╪═──', ' ╪══ ', ' ╪══─',
        '─╪═─ ', '─╪═──', '─╪══ ', '─╪══─',
        ' ─╪─ ', ' ─╪──', ' ─╪═ ', ' ─╪═─',
        ' ═╪─ ', ' ═╪──', ' ═╪═ ', ' ═╪═─',
        '──╪─ ', '──╪──', '──╪═ ', '──╪═─',
        '─═╪─ ', '─═╪──', '─═╪═ ', '─═╪═─',
        ' ─═╪ ', ' ─═╪─',
        ' ══╪ ', ' ══╪─',
        '──═╪ ', '──═╪─',
        '─══╪ ', '─══╪─',
    }),
    ((0, 1, 2, 3, 4, 5, 6), 20, {
        '    ───════╪═───    ', '    ───════╪═────   ', '    ───════╪══──    ',
        '    ───════╪══───   ', '    ───═══╪══───    ', '    ───═══╪══────   ',
        '    ───═══╪═══──    ', '    ───═══╪═══───   ', '    ───══╪═══───    ',
        '    ───══╪═══────   ', '    ───══╪════──    ', '    ───══╪════───   ',
        '    ──═════╪═───    ', '    ──═════╪═────   ', '    ──═════╪══──    ',
        '    ──═════╪══───   ', '    ──════╪══───    ', '    ──════╪══────   ',
        '    ──════╪═══──    ', '    ──════╪═══───   ', '    ──═══╪═══───    ',
        '    ──═══╪═══────   ', '    ──═══╪════──    ', '    ──═══╪════───   ',
        '   ────════╪═───    ', '   ────════╪═────   ', '   ────════╪══──    ',
        '   ────════╪══───   ', '   ────═══╪══───    ', '   ────═══╪══────   ',
        '   ────═══╪═══──    ', '   ────═══╪═══───   ', '   ────══╪═══───    ',
        '   ────══╪═══────   ', '   ────══╪════──    ', '   ────══╪════───   ',
        '   ───═════╪═───    ', '   ───═════╪═────   ', '   ───═════╪══──    ',
        '   ───═════╪══───   ', '   ───════╪══───    ', '   ───════╪══────   ',
        '   ───════╪═══──    ', '   ───════╪═══───   ', '   ───═══╪═══───    ',
        '   ───═══╪═══────   ', '   ───═══╪════──    ', '   ───═══╪════───   ',
    }),
])
def test_box_plot2_rows(dist, width, rows):
    canvas = charts.Range(dist[0], dist[-1])
    dist = charts.Distribution(*dist[1:-1])
    assert charts._box_plot2_rows(canvas, dist, width) == rows


@pytest.mark.parametrize('col,step,dist,chars', [
    (0, 1, (0, 1, 2, 3, 4, 5, 6), ' ─'),
    (1, 1, (0, 1, 2, 3, 4, 5, 6), ' ─═╪'),
    (2, 1, (0, 1, 2, 3, 4, 5, 6), '─═╪═'),
    (3, 1, (0, 1, 2, 3, 4, 5, 6), '═╪═─'),
    (4, 1, (0, 1, 2, 3, 4, 5, 6), '╪═─ '),
    (5, 1, (0, 1, 2, 3, 4, 5, 6), '─ '),
    (6, 1, (0, 1, 2, 3, 4, 5, 6), ' '),
    (7, 1, (0, 1, 2, 3, 4, 5, 6), ''),
    (0, 1, (2, 3, 4, 5, 6, 7, 8), ''),
    (1, 2, (0, 2, 4, 6, 8, 10, 12), ' ─═╪'),
])
def test_box_plot2_char(col, step, dist, chars):
    canvas = charts.Range(dist[0], dist[-1])
    dist = charts.Distribution(*dist[1:-1])
    assert ''.join(charts._box_plot2_char(col, step, canvas, dist)) == chars


@pytest.mark.parametrize('dist,width,chars', [
    ((0, 1, 2, 3, 4, 5, 6), 5, ' ─,─═╪,═╪═,╪═─,─ '),
    ((0, 1, 2, 3, 4, 5, 6), 20,
     ' , , , ─,─,─,─═,═,═,═╪═,═╪═,╪═,═,═─,─,─,─ , , , '),
])
def test_box_plot2_chars(dist, width, chars):
    canvas = charts.Range(dist[0], dist[-1])
    dist = charts.Distribution(*dist[1:-1])
    assert charts._box_plot2_chars(canvas, dist, width) == chars


@pytest.mark.parametrize('start1,end1,start2,end2,included', [
    (0, 0, 1, 1, False),
    (0, 1, 0, 1, True),
    (0, 1, 1, 2, True),
    (0, 1, 2, 3, False),
    (0, 1, 0.9, 1.5, True),
    (2, 3, 0, 1, False),
    (0.9, 1.1, 0, 1, True),
])
def test_includes(start1, end1, start2, end2, included):
    assert charts.includes(start1, end1, start2, end2) == included


@pytest.mark.parametrize('row,step,dist,error', [
    (' ─═╪═─ ', 1, (1, 2, 3.5, 5, 6), 0),
    (' ─═╪═─ ', 2, (2, 4, 7, 10, 12), 0),
    (' ─═╪═─ ', 1, (1, 2, 3, 4, 5), 0.3535533905932738),
])
def test_box_plot2_error(row, step, dist, error):
    dist = charts.Distribution(*dist)
    assert charts._box_plot2_error(dist, step)(row) == error


@pytest.mark.parametrize('row,step,dist', [
    # TODO: empty string should return all 0s?
    #('', 1, (0, 0, 0.5, 1.0, 1.0)),
    (' ─═╪═─', 1, (1, 2, 3.5, 5, 6)),
    ('──═══╪════─────', 1, (0, 2, 5.5, 10, 15)),
    (' ─═╪═─', 2, (2, 4, 7, 10, 12)),
    (' ─╪─', 1, (1, 2, 2.5, 3, 4)),
    (' ═╪═', 1, (1, 1, 2.5, 4, 4)),
])
def test_rendered_dist(row, step, dist):
    assert charts.Distribution.from_box_plot(row, step) \
        == charts.Distribution(*dist)


@pytest.mark.parametrize('dist,widths', [
    ([1, 2, 3.5, 5, 6], [1, 1.5, 1.5, 1]),
    ([1, 2, 3, 4, 5], [1, 1, 1, 1]),
])
def test_widths(dist, widths):
    assert charts.Distribution(*dist).widths == widths


@pytest.mark.parametrize('v1,v2,err', [
    ([0], [0], 0),
    ([0], [1], 1),
    ([1], [0], 1),
    ([2], [0], 2),
    ([0, 0], [0, 0], 0),
    ([0, 0], [1, 1], 1),
    ([1, 1.5, 1.5, 1], [1, 1, 1, 1], 0.3535533905932738),
])
def test_rms(v1, v2, err):
    assert charts.rms(v1, v2) == err


@pytest.mark.parametrize('origin,edge,width,step', [
    (0, 5, 5, 1),
    (0, 5, 20, 0.25),
    (0, 20, 5, 4),
])
def test_step(origin, edge, width, step):
    assert charts.Range(origin, edge).step(width) == step


@pytest.mark.parametrize('origin,edge,width,scale', [
    (0, 5, 5, 1),
    (0, 5, 20, 1),
    (0, 20, 5, 0.25),
])
def test_step(origin, edge, width, scale):
    assert charts.Range(origin, edge).scale(width) == scale
