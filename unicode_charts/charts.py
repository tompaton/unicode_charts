# coding: utf-8

import math
import re
import statistics

from collections import namedtuple


def bar_chart(data, min_value=None, max_value=None):
    if not data:
        return ''

    if min_value is None:
        min_value = min(data)

    if max_value is None:
        max_value = max(data)

    bars = '▁▂▃▄▅▆▇█'
    if min_value == 0:
        bars = ' ' + bars

    # print(data)

    scale = (len(bars) - 1) / (max_value - min_value)
    # print((min_value, max_value, scale))

    scaled = [scale * (value - min_value) for value in data]
    # print(scaled)

    # small delta to get more symmetrical rounding
    rounded = [round(i + 0.00001) for i in scaled]
    # print(rounded)

    return ''.join(bars[int(i)] for i in rounded)


def box_plot(data, mark=None):
    # https://github.com/ewheeler/ascii-boxplot/blob/master/lib/boxplot.py
    # https://github.com/hville/ascii-boxplot/blob/master/boxplot.js
    ' │─┼═╪'
    ' ──══╪──'

    if not data:
        return ''

    dist = Distribution.from_data(data)
    canvas = Range.from_dist(dist)
    scale = canvas.scale(20)
    row = _box_plot_row(canvas * scale, dist * scale)

    return _mark_box_plot_row(row, canvas.mark_pos(mark))


def heat_map(data):
    '░▒▓█◊▴'
    '░▒◊█▒▒░░░'
    return ''


def _box_plot_row(canvas, dist):
    return (' ' * (dist.minimum - canvas.origin)
            + '─' * (dist.q1 - dist.minimum)
            + '═' * (dist.median - dist.q1)
            # TODO: this is adding an extra character which throws off mark...
            # probably need to add it in after the 4 quartiles, replacing the
            # "middle" character
            + '╪'
            + '═' * (dist.q3 - dist.median)
            + '─' * (dist.maximum - dist.q3)
            + ' ' * (canvas.edge - dist.maximum))


def _mark_box_plot_row(row, mark):
    # print(mark)

    if mark is None:
        return row

    assert 0.0 <= mark <= 1.0

    # '̭͎͓̻̥̊̽̌' (or similar) to indicate current sample
    index = int(round(mark * len(row))) or 1

    return '{}̊{}'.format(row[:index], row[index:])


class Range(namedtuple('Range', 'origin edge')):

    def __mul__(self, scale):
        return type(self)(*[int(round(val * scale)) for val in self])

    def step(self, width):
        return (self.edge - self.origin) / width

    def scale(self, width):
        return min(1.0, width / (self.edge - self.origin))


    def mark_pos(self, mark):
        # print(mark, self.origin, self.edge)

        if mark is not None and self.origin <= mark <= self.edge:
            return (mark - self.origin) / (self.edge - self.origin)
        else:
            return None

    @classmethod
    def from_dist(cls, dist):
        assert 0 <= dist.minimum <= dist.maximum

        delta = dist.maximum - dist.minimum

        for divisions in [1, 2, 5]:
            tick_size = pow(10, math.ceil(math.log10(1 + delta))) / divisions

            origin = tick_size * math.floor((dist.minimum - 0.01) / tick_size)
            edge = tick_size * math.ceil((dist.maximum + 0.01) / tick_size)

            # if too much space, shrink "tick size"
            usage = delta / (edge - origin)
            # print(divisions, usage)
            if not delta or usage > 0.5:
                break

        # if close to 0, make it zero
        # print(delta and origin/delta)
        if delta and origin / delta < 0.5:
            origin = 0

        return cls(max(0, origin), edge)


class Distribution(namedtuple('Distribution', 'minimum q1 median q3 maximum')):

    def __mul__(self, scale):
        return type(self)(*[int(round(val * scale)) for val in self])

    @property
    def widths(self):
        return [b - a for a, b in zip(self, self[1:])]

    @classmethod
    def from_data(cls, data):
        data = sorted(data)

        length = len(data)
        minimum = data[0]
        median = statistics.median(data)
        maximum = data[length - 1]

        length_2 = length // 2 or 1
        q1 = statistics.median(data[:length_2])
        q3 = statistics.median(data[-length_2:])

        return cls(minimum, q1, median, q3, maximum)

    @classmethod
    def from_box_plot(cls, row, step):
        minimum = q1 = 0

        left, mid, right = row.partition('╪')

        for col in left:
            if col == ' ':
                minimum += step
                q1 += step
            if col == '─':
                q1 += step

        median = (len(left) + 0.5) * step

        q3 = maximum = median + (0.5 * step)

        for col in right:
            if col == '═':
                q3 += step
                maximum += step
            if col == '─':
                maximum += step

        return cls(minimum, q1, median, q3, maximum)


def box_plot2(data):
    if not data:
        return ''

    dist = Distribution.from_data(data)
    canvas = Range.from_dist(dist)
    return _box_plot_row2(canvas, dist, mark=None, width=20)


def _box_plot_row2(canvas, dist, mark=None, width=20):
    rows = sorted(_box_plot2_rows(canvas, dist, width))
    step = canvas.step(width)
    return min(rows, key=_box_plot2_error(dist, step))


def _box_plot2_rows(canvas, dist, width):
    rows = ['']

    step = canvas.step(width)

    while len(rows[0]) < width:
        col = len(rows[0])
        rows_ = rows
        rows = []
        for char in sorted(set(_box_plot2_char(col, step, canvas, dist))):
            for row in rows_:
                rows.append(row + char)

    valid = re.compile(' *─*═*╪═*─* *')
    return set([row for row in rows if valid.fullmatch(row)])


def _box_plot2_chars(canvas, dist, width):
    step = canvas.step(width)
    return ','.join(''.join(_box_plot2_char(col, step, canvas, dist))
                    for col in range(width))


def _box_plot2_char(col, step, canvas, dist):
    start = col * step
    end = (col + 1) * step

    if includes(canvas.origin, dist.minimum, start, end):
        yield ' '

    if includes(dist.minimum, dist.q1, start, end):
        yield '─'

    if includes(dist.q1, dist.median, start, end):
        yield '═'

    if includes(dist.median - step, dist.median + step, start, end):
        yield '╪'

    if includes(dist.median, dist.q3, start, end):
        yield '═'

    if includes(dist.q3, dist.maximum, start, end):
        yield '─'

    if includes(dist.maximum, canvas.edge, start, end):
        yield ' '


def includes(start1, end1, start2, end2):
    return end2 >= start1 and start2 <= end1


def _box_plot2_error(dist, step):
    def inner(row):
        return rms(Distribution.from_box_plot(row, step).widths, dist.widths)
    return inner


def rms(v1, v2):
    return math.sqrt(sum((v1_ - v2_)**2
                         for v1_, v2_ in zip(v1, v2))
                     / len(v1))
