import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="unicode_charts",
    version="1.0.0",
    author="Tom Paton",
    author_email="tom.paton@gmail.com",
    description="Generate simple ASCII art charts using unicode characters",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://bitbucket.com/tompaton/unicode_charts",
    packages=setuptools.find_packages(),
    classifiers=(
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ),
    install_requires=[
        'pytest',
    ],
)
